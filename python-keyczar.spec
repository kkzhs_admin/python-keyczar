%global _description\
Keyczar is an open source cryptographic toolkit designed to make it easier and\
safer for developers to use cryptography in their applications. Keyczar\
supports authentication and encryption with both symmetric and asymmetric keys.

Name:           python-keyczar
Version:        0.71c
Release:        15
Summary:        Toolkit for safe and simple cryptography
License:        ASL 2.0
URL:            https://pypi.org/project/python-keyczar
Source0:        https://github.com/ben-prezola/keyczar-python2to3/archive/master.zip
Patch0:         0001-fit-to-python3.patch
BuildArch:      noarch

BuildRequires:  python3-devel python3-crypto python3-pyasn1

%description %_description

%package -n python3-keyczar
Summary:        %summary
Requires:       python3-crypto python3-pyasn1
%{?python_provide:%python_provide python2-keyczar}

%description -n python3-keyczar %_description

%prep
%autosetup -n keyczar-python2to3-master -p1
rm -rf python_keyczar.egg-info
chmod 744 keyczar/test/__init__.py

%build
sed -i -e '/use_2to3/s/\S.*/pass/' setup.py
%{__python3} setup.py build

%check
cd keyczar/test
PYTHONPATH=$PYTHONPATH:../ ./__init__.py

%install
%{__python3} setup.py install -O1 --skip-build --root ${RPM_BUILD_ROOT}

%files -n python3-keyczar
%doc README doc/pycrypt.pdf
%license LICENSE
%{python3_sitelib}/keyczar/
%{python3_sitelib}/python_keyczar-*.egg-info
%{_bindir}/keyczart


%changelog
* Sat Jan 08 2022 yaoxin <yaoxin30@huawei.com> - 0.71c-15
- Fixed the issue that python-keyczar could not recognize the parameter use_2to3 in the %build stage due to the upgrade of  python-setuptools to version 59.4.0

* Fri Oct 23 2020 Ge Wang <wangge20@huawei.com> - 0.71c-14
- Remove python2

* Fri Nov 15 2019 sunguoshuai <sunguoshuai@huawei.com> - 0.71c-13
- Package init
